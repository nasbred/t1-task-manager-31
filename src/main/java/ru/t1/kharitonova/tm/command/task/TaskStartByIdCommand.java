package ru.t1.kharitonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Start task by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-start-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
    }

}
