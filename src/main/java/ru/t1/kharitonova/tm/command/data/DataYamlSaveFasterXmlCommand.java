package ru.t1.kharitonova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.Domain;
import ru.t1.kharitonova.tm.enumerated.Role;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public String getDescription() {
        return "Save data to yaml file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-save-yaml-faster-xml";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE YAML]");
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_YAML);
        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper mapper = new YAMLMapper();
        @NotNull final String yaml = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(yaml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
