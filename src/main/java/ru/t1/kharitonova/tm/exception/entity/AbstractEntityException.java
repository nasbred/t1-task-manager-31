package ru.t1.kharitonova.tm.exception.entity;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.exception.AbstractException;

@NoArgsConstructor
public class AbstractEntityException extends AbstractException {

    public AbstractEntityException(@NotNull final String message) {
        super(message);
    }

    public AbstractEntityException(@NotNull final String message,
                                   @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractEntityException(@NotNull final String message,
                                   @NotNull final Throwable cause,
                                   final boolean enableSuppression,
                                   final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
